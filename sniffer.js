var utils = require("./libs/utils")
	, fs = require("fs")
	, path = require("path")
	, config = require("./config/config.json")
	, log = utils.log(config.logLevel);

module.exports = function(actions){

	// Revisamos las menciones
	var Mentions = require("./libs/Mentions.js")(config)
		, last_mention = 0;
	
	Mentions.get(function(err, data, response){
		if (err) throw err;
		if (!data || data.length == 0) return;

		// Por cada mención devuelta
		data.forEach(function(item){
			if (last_mention == item.id) return; // Corrección de mensajes de twitter

			var text = item.text.toLowerCase().match(/\@\w+\s+(\w+)/);

			log.debug("Message from ("+item.user.screen_name+" ): " + item.text);
			log.debug(text);

			if (text && text[1]){
				try {
					actions[text[1]].process(item);
				} catch(e){
					log.error("Action " + text[1] + " not found");
				}
			
			}
		});

		Mentions.update(data);
	});
};