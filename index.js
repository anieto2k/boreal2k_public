var config = require("./config/config.json")
	, path = require("path")
	, fs = require("fs")
	, log = require("./libs/utils").log(config.logLevel);

/**
	Cargamos las acciones disponibles
*/
var controller_dir = path.join(__dirname,'/actions/');
fs.readdir(controller_dir, function(err, files){
	if (err){
		log.error("Se ha detectado un error al leer el directorio " + controller_dir);
		return;
	}

	// Cargamos acciones disponibles
	var actions = {};
	files
		.filter(function(file) { return file.substr(-3) == '.js'; })
		.forEach(function(file){
			try {
				var action = require(controller_dir + file)(config);
				actions[action.name] = action;
			}catch(e){
				log.error("Error al cargar la acción: " + file);
				console.error(e);
			}			
	});


	// Ejecutamos sniffer de mentions
	require("./sniffer.js")(actions);
});