var i18n = require("../i18n/messages.json")
	, utils = require("../libs/utils");

module.exports = function(_config){
	var config = _config;

	var mongo2k = require("../libs/Mongo2k")(config)
		, bot2k = require("../libs/Bot2k")(config)
		, log = utils.log(config.logLevel);

	/**
	 * Eliminamos un usuario de la BD
	 * @param  {[type]}   user     [description]
	 * @return {[type]}            [description]
	 */
	function unregister(user){
		mongo2k.find(mongo2k.collections.users, {username: user.username}, function(err, data){
			if (err) throw err;
			debugger;
			log.debug("BORRAMOS USUARIO " + user.username);
			if (data && data.length > 0){
				mongo2k.remove(mongo2k.collections.users, user, function(err, data){
					if (err) throw err;
					bot2k.mention(user.username, {message: i18n.UNREGISTERSUCCESS})
				});
				return;
			} else {
				bot2k.mention(user.username, {message: i18n.NOREGISTERDATA});
			}
		});
	};

	return {
		name: 'unregister',
		command: 'unregister',
		usage: utils.format('Usage: %s unregister', config.botname),
		process: function(item){
			var text = item.text.toLowerCase().match(/\@\w+\s+(\w+)/);
			log.debug("Message from ("+item.user.screen_name+" ): " + item.text);
						
			if (text[1] && text[1] == 'help'){
				bot2k.mention(item.user.screen_name, {message: this.usage});
			} else {
				debugger;
				unregister({username: item.user.screen_name});
			}
		}
	};
	
};