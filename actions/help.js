var i18n = require("../i18n/messages.json")
	, utils = require("../libs/utils");

module.exports = function(_config){
	debugger;
	var config = _config;
	
	var bot2k = require("../libs/bot2k")(config);
	
	return {
		name: 'help',
		command: 'help',
		usage: utils.format('$s help', config.botname),
		process: function(item){
			bot2k.mention(item.user.screen_name, {message: i18n.HELPMESSAGE});
		}
	}
};