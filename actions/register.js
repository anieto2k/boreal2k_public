var i18n = require("../i18n/messages.json")
	, utils = require("../libs/utils");

module.exports = function(_config){
	var config = _config;

	var mongo2k = require("../libs/Mongo2k")(config)
		, bot2k = require("../libs/Bot2k")(config)
		, log = utils.log(config.logLevel);

	/**
	 * Registramos un usuario 
	 * @param  {[type]}   user     [description]
	 * @param  {Function} callback [description]
	 * @return {[type]}            [description]
	 */
	function register(user){
		mongo2k.find(mongo2k.collections.users, { username: user.username }, function(err, data){
			debugger;
			if (err) throw err;
			if (!data || data.length == 0){
				mongo2k.insert(mongo2k.collections.users, user, function(err, data){
					debugger;
					if (err) throw err;
					bot2k.mention(user.username, {message: i18n.REGISTEROK});
				});
			} else {
				bot2k.mention(user.username, {message: i18n.REGISTERFAIL});
			}
		});
	};

	return {
		name: 'register',
		command: 'register',
		usage: utils.format('Usage: %s register kp:{index} start:{starttime} end:{endtime}', config.botname),
		process: function(item){
			var text = item.text.toLowerCase().match(/register\s+(\w+)/);
			log.debug("Message from ("+item.user.screen_name+" ): " + item.text);
						

			if (text[1] && text[1] == 'help'){
				bot2k.mention(item.user.screen_name, {message: this.usage});
			} else {
				debugger;
				/**
				 * 1: KP Index (\d.)
				 * 2: Start Time: (\d:)
				 * 3: End Time: (\d:)
				 */
				text = item.text.toLowerCase().match(/register\s+kp:([\d\.]+)\s+start:([\d:]+)\s+end:([\d:]+)/);
				
				if (!text || text.length < 4){
					bot2k.mention(item.user.screen_name, {message: this.usage});
				} else {
					register({
						username: item.user.screen_name,
						kp: parseFloat(text[1]),
						start: text[2].replace(':', ''),
						end: text[3].replace(':', '')
					});
				}
			}
		}
	};
	
};