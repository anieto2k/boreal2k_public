var i18n = require("../i18n/messages.json")
	, utils = require("../libs/utils");

module.exports = function(_config){
	var config = _config;

	var mongo2k = require("./Mongo2k")(config)
		, bot2k = require("./Bot2k")(config)
		, log = utils.log(config.logLevel);

	
	var last_mention = 0;
	/**
	 * Cargamos las menciones
	 * @param  {Function} callback [description]
	 * @return {[type]}            [description]
	 */
	function getMentions(callback){
		mongo2k.find(mongo2k.collections.config, {}, function(err, cfg){
			if (err) throw err;
			var data = {}; 
			if (cfg[0] && cfg[0].last_mention){
				last_mention = data.since_id = cfg[0].last_mention;
			}
			bot2k.getMentions(data, callback);
		});
	}

	/**
	 * Actualizamos la última mención leida en Mongodb
	 * @param  {[type]} data [description]
	 * @return {[type]}      [description]
	 */
	function updateLastMention(data){
		// Actualizamos el último tweetID consultado
		mongo2k.find(mongo2k.collections.config, {}, function(err, cfg){
			debugger;
			if (err) throw err;
			if (!cfg[0] || !cfg[0].last_mention){
				mongo2k.insert(mongo2k.collections.config, {
					last_mention: data[0].id
				});
				return;
			}
			mongo2k.update(mongo2k.collections.config, {last_mention: cfg[0].last_mention}, {last_mention: data[0].id}, function(err, data){
				log.debug("Salvado");
			});
		})
	}

	return {
		get: getMentions,
		update: updateLastMention
	};
};