var   util = require("util")
	, moment = require("moment");
/**
 * Hour Parser
 * @param  {[type]} hour [description]
 * @return {[type]}      [description]
 */
util.createHour = function(hour){
	return hour.substring(0,2) + ":" + hour.substring(2)
}
/**
 * Creamos una fecha
 * @param  {[type]} item [description]
 * @return {[type]}      [description]
 */
util.createDate = function(item){
	var date = new Array();
	date.push(parseInt(item.year));
	date.push(parseInt(item.month));
	date.push(parseInt(item.day));
	date.push(parseInt(item.hour));
	date.push(parseInt(item.minutes));
	return date;
}

/**
 * Minimal Log4j
 * @param  {[type]} level [description]
 * @return {[type]}       [description]
 */
var colors = require("colors");
util.log = function(level){
	var levels = ['debug', 'info', 'warn', 'error'];

	levels = levels.slice(levels.indexOf(level || 'info'));

	function write(type, message){
		if (typeof message != 'string') message = JSON.stringify(message);
		if (levels.indexOf(type) > -1)
		console.log("[" + type.toUpperCase() + "] " + moment().format("DD/MM/YYYY HH:mm:ss ") + message);
	}

	return {
		debug: function(message){
			write("debug", message.grey)
		},
		info: function(message){
			write("info", message.cyan)
		},
		warn: function(message){
			write("warn", message.green)
		},
		error: function(message){
			write("error", message.red)
		}
	};
};

module.exports = util;;
