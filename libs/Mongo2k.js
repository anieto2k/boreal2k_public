//var MongoClient = require('mongodb').MongoClient;
var MongoClient = require("../mocks/MongoClient.js");

var Mongo2k = function(_config){
	var config = _config;

	var log = require("./utils").log(config.logLevel);

	var mongoclient = {};

	mongoclient.collections = config.database.collections;
	/**
	 * Insertamos en mongodb
	 * @param  {[type]}   collection_name [description]
	 * @param  {[type]}   data            [description]
	 * @param  {Function} callback        [description]
	 * @return {[type]}                   [description]
	 */
	mongoclient.insert = function(collection_name, data, callback){
		var collection_name = config.database.collections[collection_name];

		var callback = callback || function(err, data){};

		MongoClient.connect(config.database.url, function(err, db) {
			if(err) throw err;
			log.debug("Insertamos " + JSON.stringify(data) + " en " + collection_name);

			var collection = db.collection(collection_name);
			collection.insert(data, function(err, data){
				callback(err,data);
				db.close();
			});
		});
	};

	/**
	 * Buscamos en una colección de mongodb
	 * @param  {[type]}   collection_name [description]
	 * @param  {[type]}   data            [description]
	 * @param  {[type]}   sort            [description]
	 * @param  {Function} callback        [description]
	 * @return {[type]}                   [description]
	 */
	mongoclient.find = function(collection_name, data, sort, callback){
		var collection_name = config.database.collections[collection_name];
		if (typeof sort == 'function') callback = sort;

		MongoClient.connect(config.database.url, function(err, db) {
			if(err) throw err;
			log.debug("Buscamos " +  JSON.stringify(data) + " en " + collection_name);
			
			var collection = db.collection(collection_name);

			var cursor = collection.find(data);
			if (sort != callback)
				cursor.sort(sort);

			cursor.toArray(function(err, results) {
					callback(err,results);
					db.close();
				});
		});
	};

	/**
	 * Actualizamos un dato de mongodb
	 * @param  {[type]}   collection_name [description]
	 * @param  {[type]}   query           [description]
	 * @param  {[type]}   data            [description]
	 * @param  {Function} callback        [description]
	 * @return {[type]}                   [description]
	 */
	mongoclient.update = function(collection_name, query, data, callback){
		var collection_name = config.database.collections[collection_name];
		MongoClient.connect(config.database.url, function(err, db) {
			if(err) throw err;
			log.debug("Actualizamos " +  JSON.stringify(query) + " por  " + JSON.stringify(data) + " en " + collection_name);
			
			var collection = db.collection(collection_name);
			collection.update(query, {
				 $set: data
			},function(err, results) {
				callback(err,results);
				db.close();
			});
		});
	};

	/**
	 * Eliminamos un elemento de mongodb
	 * @param  {[type]}   collection_name [description]
	 * @param  {[type]}   query           [description]
	 * @param  {Function} callback        [description]
	 * @return {[type]}                   [description]
	 */
	mongoclient.remove = function(collection_name, query, callback){
		var collection_name = config.database.collections[collection_name];
		MongoClient.connect(config.database.url, function(err, db) {
			if(err) throw err;
			log.debug("Borramos " +  JSON.stringify(query) + " en " + collection_name);
		
			var collection = db.collection(collection_name);
			collection.remove(query, 1 ,function(err, results) {
				callback(err,results);
				db.close();
			});
		});
	}
	return mongoclient;
};

module.exports = Mongo2k;