var  Twit = require('twit')
	, extend = require("extend");


var Bot2k = function(_config){
	var bot2k = {};
	var config = extend({}, _config);
	var log = require("./utils").log(config.logLevel);
	
	// var client = new Twit(config.twitter);
	// Ciente Dummy
	var client = require("../mocks/TwitClient.js");

	/**
	 * Actualizamos el timeline publico
	 * @param  {[type]}   str      [description]
	 * @param  {Function} callback [description]
	 * @return {[type]}            [description]
	 */
	bot2k.update = function(str, callback){
		log.debug("Twitteamos: " + str);

		var callback = callback || function(){};

		client.post('statuses/update', { status: str }, callback);
	}

	/**
	 * Obtenemos las menciones de @boreal2k
	 * @param  {[type]} data [description]
	 * @return {[type]}      [description]
	 */
	bot2k.getMentions = function(data, callback){
		log.debug("Solicitamos las menciones: " + JSON.stringify(data));
	
		var callback = callback || function(){};
		//if (config.debug) return callback(null, {debug: true}, {response: "none"});

		client.get('statuses/mentions_timeline', data, callback);
	}

	/**
	 * Publicamos un mensaje directo a un usuario
	 * @param  {[type]}   username [description]
	 * @param  {[type]}   str      [description]
	 * @param  {Function} callback [description]
	 * @return {[type]}            [description]
	 */
	bot2k.direct_message = function(username, str, callback){
		log.debug("Mensaje privado al usuario " + username + ": " + str);
		
		var callback = callback || function(){};

		client.post('direct_messages/new', { screen_name: username, text: str }, callback);
	}

	/**
	 * Publicamos una mención a un usuario concreto
	 * @param  {[type]}   username [description]
	 * @param  {[type]}   data     [description]
	 * @param  {Function} callback [description]
	 * @return {[type]}            [description]
	 */
	bot2k.mention = function(username, data, callback){
		log.debug("Mention: " + username + " -- " + JSON.stringify(data));
		this.update("@" + username + " " + data.message, function(err, data){
			log.debug("TWEET ENVIADO");
		});
	}

	return bot2k;
}
module.exports = Bot2k;
