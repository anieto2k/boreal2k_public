var colors = require("colors");

function print(str){
	return str.magenta;
}

var collection = function(name){
	console.log(print("\t + Collection: " + name));

	return {
		insert: function(data, callback){
			console.log(print("\t\t + Insertamos: " + JSON.stringify(data)));
			callback(null, data);
		},
		find: function(data){
			console.log(print("\t\t + Buscamos: " + JSON.stringify(data)));
			return {
				sort: function(data){
					console.log(print("\t\t ++ Ordenamos: " + JSON.stringify(data)));
				},
				toArray: function(fn){
					console.log(print("\t\t ++ toArray "));
					fn(null, {});
				}
			}
		},
		update: function(data){
			console.log(print("\n\t\t + Update: " + JSON.stringify(data)));
			callback(null, data);
		},
		remove: function(data){
			console.log(print("\n\t\t + Borramos: " + JSON.stringify(data)));
			callback(null, data);
		}
	}
}


module.exports = {
	connect: function(url, fn){
		console.log(print("[Mongo2b][connect]\n\t URL:" + url));
		fn(null, {
			collection: collection,
			close: function(){
				console.log(print("\t\t + Cerramos conexion "));
			}
		})
	}
}

