var colors = require("colors");

var print = function(str){
	return str.green;
}

module.exports = {
		post: function(where, data, fn){
			console.log(print("[DemoBot][POST]\n\tURL: " + where + " \n\tData: " + JSON.stringify(data)));
			fn(null, []);
		},
		get: function(where, data, fn){
			console.log(print("[DemoBot][GET]\n\tURL: " + where + " \n\tData: " + JSON.stringify(data)));
			fn(null, [{
				id: 1,
				text: "@boreal2k help",
				user: {
					screen_name: "aNieto2k"
				}
			},
			{
				id: 2,
				text: "@boreal2k register kp:2 start:1700 end:0800",
				user: {
					screen_name: "aNieto2k"
				}
			},
			{
				id: 3,
				text: "@boreal2k unregister",
				user: {
					screen_name: "aNieto2k"
				}
			}]);
		}
	};